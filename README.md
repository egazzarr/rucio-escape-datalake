Documentation for new users on how to authenticate with the correct certificates, interact with the Rucio Client and access the DLaaS platform.
To be used as a platform to integrate and unify the ESCAPE WP2 documentation.
Navigate to https://rucio-datalake.docs.cern.ch/.
